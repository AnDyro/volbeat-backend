Rails.application.routes.draw do
      root 'users#show'
      #get 'users/'
      post "/graphql", to: "graphql#execute"
      require 'sidekiq/web'
      if Rails.env.development?
        mount GraphiQL::Rails::Engine, at: "/graphiql", graphql_path: "/graphql"
      end
      resources :users, only: [:index]
      match "*unmatched", via: [:options], to: "graphql#options_request"
      mount Sidekiq::Web => '/sidekiq'
      # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.htm
end
  