class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.string :name
      t.string :last_name
      t.string :email
      t.string :status
      t.string :password_digest
      t.datetime :last_signed
      t.string :last_ip
      t.integer :signed_count
      t.string :website
      t.string :charge

      t.timestamps
    end
  end
end
