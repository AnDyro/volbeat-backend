class CreateSendMails < ActiveRecord::Migration[5.1]
  def change
    create_table :send_mails do |t|
      t.string :name
      t.string :email
      t.string :comment

      t.timestamps
    end
  end
end
