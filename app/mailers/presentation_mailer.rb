class PresentationMailer < ApplicationMailer
    default from: 'diego.cabrera@volbeatsi.com'
    
     def send_presentation(user)
       @user = user
      #  @url  = 'http://example.com/login'
       mail(to: @user.email, subject: 'Volbeat - Descargar presentación',content_type: "text/html",name:"Equipo Volbeat")
     end
end
