class ApplicationMailer < ActionMailer::Base
  # default from: 'angelmendezz@kovle.com'
  layout 'mailer'
end
