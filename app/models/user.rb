require 'elasticsearch/model'

class User < ApplicationRecord
    include Elasticsearch::Model
    include Elasticsearch::Model::Callbacks
    before_create :assign_default_role
    after_save :index_user_in_elasticsearch
    rolify
    has_secure_password
    has_many :tokens, dependent: :destroy

    validates :name, presence: true, length: { in: 3..100 }
    validates :last_name, presence: true, length: { in: 3..100 }
    validates :email, presence: true, length: { in: 3..100 }, uniqueness: true
    validates :charge, presence: true, length: { in: 3..100 }

    settings do
        mappings dynamic: false do
          indexes :name, type: :text
          indexes :last_name, type: :text
          indexes :email, type: :text
          indexes :website, type: :text
          indexes :charge, type: :text
        end
    end

    private
        def assign_default_role
            self.add_role(:employe) if self.roles.blank?
        end
        def index_user_in_elasticsearch
            self.__elasticsearch__.index_document
        end
end
