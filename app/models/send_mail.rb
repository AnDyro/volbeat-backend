class SendMail < ApplicationRecord

    validates :name, presence: true, length: { in: 5..100 }
    validates :email, presence: true, length: { in: 4..100}

end
