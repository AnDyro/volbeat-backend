class UsersController < ApplicationController
  def index
    response = User.__elasticsearch__.search(
      query: {
        query_string: {
          query: "#{params[:query]}* AND (status:active)",
        },
      }
    ).results
    render json: {
      results: response.results,
      total: response.total
    }
  end

  def show
    return render json: {data: "invalid hash"}
  end
end
