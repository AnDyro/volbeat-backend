Types::QueryType = GraphQL::ObjectType.define do
  name "Query"
  # Add root-level fields here.
  # They will be entry points for queries on your schema.

  # TODO: remove me
  field :currentUser, !Types::UserType do
    description "Current User info"
    resolve ->(obj, args, ctx) {
      current_user = ctx[:current_user]
      return GraphQL::ExecutionError.new("Usuario no encontrado") if current_user.nil?
      current_user
    }
  end

  field :showAllUsers, types[Types::UserType] do
    description "Show all users"
    resolve ->(obj, args, ctx) {
      current_user = ctx[:current_user]
      return GraphQL::ExecutionError.new("Necesitas una cuenta para ver los usuarios") if current_user.nil?
      has_admin = current_user.has_role? :admin
      return GraphQL::ExecutionError.new("No puedes realizar esta acción") unless has_admin
      users = User.all.includes(:roles).limit(20)
    }      
  end

  field :searchAll, types.String do
    # argument :search, types.String
    resolve ->(obj, args, ctx) {
      User.__elasticsearch__.search(args[:query]).results
    }
  end
end
