Types::SendPresentationInput = GraphQL::InputObjectType.define do
    name 'SEND_PRESENTATION_INPUT'
    argument :name, !types.String
    argument :email, !types.String
    argument :comment, types.String
  end