Types::CreateUserInput = GraphQL::InputObjectType.define do
    name 'CREATE_USER_INPUT'
    argument :name, !types.String
    argument :last_name, types.String
    argument :website, types.String
    argument :charge, !types.String
    argument :email, !types.String
    argument :password, !types.String
  end