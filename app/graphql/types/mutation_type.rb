Types::MutationType = GraphQL::ObjectType.define do
  name "Mutation"
  #Contact Form
  field :SendPresentation, function: Resolvers::SendPresentation.new 
  # field :showSendPresentations, function: Resolvers::ShowSendPresentations.new 
  
  #Members
  field :createUser, function: Resolvers::CreateUser.new 
  # field :createAdmin, function: Resolvers::CreateAdmin.new 
  # field :createEmploye, function: Resolvers::CreateEmploye.new 
  field :signInUser, function: Resolvers::SignInUser.new 
end
