Types::UserType = GraphQL::ObjectType.define do
    name "Member"

    field :name, !types.String
    field :last_name, !types.String
    field :email, !types.String
    field :website, types.String
    field :charge, !types.String
    field :roles, !types[Types::RoleType], "Role" do
        resolve->(obj, args, ctx){
            obj.roles
        }
    end
    field :has_admin, !types.Boolean, "Is admin" do
        resolve->(obj, args, ctx){
            obj.has_role? :admin
        }
    end

end
  