class Resolvers::CreateUser < GraphQL::Function

    argument :data, !Types::CreateUserInput
    type do
        name 'CreateUserPayload'
        field :user, !Types::UserType
    end
    def call(_obj, args, ctx)
        @user = User.new(
            name: args[:data][:name], 
            last_name: args[:data][:last_name],
            email: args[:data][:email],
            password: args[:data][:password],
            charge: args[:data][:charge],
            signed_count: 0
        )
        if(@user.save)
            # SendNewUserEmailJob.perform_later(@user)
        else
            return GraphQL::ExecutionError.new("Error: Intenta de nuevo")
        end
        user = @user
        # SendPresentationJob.perform_now()
        OpenStruct.new({
            user: user
        })
    end    
end