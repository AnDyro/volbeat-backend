class Resolvers::SignInUser < GraphQL::Function
    argument :data, !Types::AuthProviderEmailInput
    type do
        name 'SignInPayload'
        field :token, types.String
        field :user, Types::UserType
    end

    def call(_obj, args, ctx)
        input = args[:data]
        return GraphQL::ExecutionError.new("Parámetros incorrectos") unless input
        user = User.find_by(email: input[:email])
        return GraphQL::ExecutionError.new("Usuario incorrecto") unless user
        return GraphQL::ExecutionError.new("Error al iniciar sesión") unless user.try(:authenticate,input[:password])
        crypt = ActiveSupport::MessageEncryptor.new(Rails.application.secrets.secret_key_base.byteslice(0..31))
        token = crypt.encrypt_and_sign("user-id:#{ user.id }")
        user.tokens.create!(token: token)
        UpdateSignInCountJob.perform_later(user)
        OpenStruct.new({
            user: user,
            token: token
        })
    end
    
end