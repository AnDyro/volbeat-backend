class Resolvers::SendPresentation < GraphQL::Function
    # name "SendPresentation"
    argument :data, !Types::SendPresentationInput
    # TODO: Remove me
    type do
        name 'CreateTextPayload'
        field :res, !types.String
    end
    def call(_obj, args, ctx)
        @userMail = SendMail.new(
            name: args[:data][:name],
            email: args[:data][:email],
            comment: args[:data][:comment]
        ) 
        if(@userMail.save)
            SendPresentationJob.perform_later(@userMail)
        else
            return GraphQL::ExecutionError.new("Error: Intenta de nuevo")
        end
        res = @userMail.attributes
        # SendPresentationJob.perform_now()
        OpenStruct.new({
            res: res
        })
    end    
end