class SendPresentationJob < ApplicationJob
  queue_as :default

  def perform(user)
    PresentationMailer.send_presentation(user).deliver_now
  end
end
